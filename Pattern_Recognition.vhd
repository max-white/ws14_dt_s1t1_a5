library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;  
    use ieee.std_logic_unsigned.all;

	
	
	
entity Pattern_Recognition is
	port(
	    dip1 : out std_logic_vector(1 downto 0);
	    dip2 : out std_logic_vector(1 downto 0);
	    dip3 : out std_logic_vector(1 downto 0);
	    dip4 : out std_logic_vector(1 downto 0);
	    dip5 : out std_logic_vector(1 downto 0);
	    dip6 : out std_logic_vector(1 downto 0);
	    --
	    do	: out std_logic_vector(1 downto 0);
	    mrk	: out std_logic;
	    --
	    di	: in std_logic_vector(1 downto 0);
	    --
	    clk	 : in std_logic;
	    nres : in std_logic
	);
end entity Pattern_Recognition;

