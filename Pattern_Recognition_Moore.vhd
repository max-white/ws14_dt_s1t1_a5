library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;  
    use ieee.std_logic_unsigned.all;



architecture Pattern_Recognition_Moore of Pattern_Recognition is

	signal di_cs  : std_logic_vector(1 downto 0) := "00";
	signal di_ns  : std_logic_vector(1 downto 0);
	signal z_cs	  : std_logic_vector(2 downto 0) := "000"; -- Ein Bit mehr als bei Mealy, weil Moore einen Zustand mehr hat.
	signal z_ns	  : std_logic_vector(2 downto 0); 
	signal mrk_cs : std_logic := '0'; 
	signal mrk_ns : std_logic;
    signal do_cs  : std_logic_vector(1 downto 0) := "00";	
	-- Debug Shift Register:
	-- signal dip1_cs : std_logic_vector(1 downto 0); -- nicht noetig, da direkte Zuweisung von di_cs auf "dip1" (und dip2_cs).
	signal dip2_cs	: std_logic_vector(1 downto 0) := "00";
	signal dip3_cs	: std_logic_vector(1 downto 0) := "00";
	signal dip4_cs	: std_logic_vector(1 downto 0) := "00";
	signal dip5_cs	: std_logic_vector(1 downto 0) := "00";
	signal dip6_cs	: std_logic_vector(1 downto 0) := "00";
	
	
begin

	di_ns <= di;

	USN:
	process(di_cs, z_cs) is
	
	    variable z_v	: std_logic_vector(2 downto 0);
	
	begin
	
        case z_cs is
	
		    when "000" => -- Z 0
			    if (di_cs = "11") then
				    z_v := "001";
			    else
				    z_v := "000";
			    end if;
			
		    when "001" => -- Z 11
			    if(di_cs = "11") then
				    z_v := "010";
			    else
				    z_v := "000";
			    end if;
			
		    when "010" => -- Z 11 11
			    if(di_cs = "00") then
				    z_v := "011";
				elsif(di_cs = "11") then
					z_v := "010";
				else
					z_v := "000";
				end if;
			
			when "011" => -- Z 11 11 00
				if(di_cs = "00") then
					z_v := "100";
				elsif(di_cs = "11") then
					z_v := "001";
				else
					z_v := "000";
				end if;
			
			when "100" => -- Z 11 11 00 00
				if(di_cs = "11") then
					z_v := "001";
				else
					z_v := "000";
				end if;
			
			when others => z_v := "000";
		end case;

		z_ns <= z_v;

	end process USN; 

	
	ASN:
	process (z_cs) is
	
	    variable mrk_v	: std_logic;
	
	begin

    	if( z_cs = "100" ) then
			mrk_v := '1';
		else
			mrk_v := '0';
		end if;
		
	mrk_ns <= mrk_v;
	
	end process ASN;

	
	
	
	Reg:
	process (clk) is
	begin
		if(clk'event and clk='1') then
			if(nres = '0') then
				z_cs   <= (others => '0');
				mrk_cs <= '0';
				di_cs  <= (others => '0');
				do_cs  <= (others => '0');
			else
				z_cs   <= z_ns;
				mrk_cs <= mrk_ns;
				di_cs  <= di_ns;
				do_cs  <= di_cs;
			end if;
		end if;
	end process Reg;


	Debug_Shift_Registers:
	process (clk) is
	begin
		if(clk'event and clk='1') then
			if(nres = '0') then
				dip2_cs <= (others => '0');
				dip3_cs <= (others => '0');
				dip4_cs <= (others => '0');
				dip5_cs <= (others => '0');
				dip6_cs <= (others => '0');
			else
				dip2_cs <= di_cs;
				dip3_cs	<= dip2_cs;
				dip4_cs	<= dip3_cs;
				dip5_cs	<= dip4_cs;
				dip6_cs <= dip5_cs;
			end if;
		end if;
	end process Debug_Shift_Registers;

	
	dip1 <= di_cs;
	dip2 <= dip2_cs;
	dip3 <= dip3_cs;
	dip4 <= dip4_cs;
	dip5 <= dip5_cs;
	dip6 <= dip6_cs;
    --
	mrk <= mrk_cs;
	do  <= do_cs;

end architecture Pattern_Recognition_Moore;
