-- Code belongs to TI3 DT WS 11/12
-- Demo f�r Z1
-- History:
--  111212: 1st version for TI3 DT WS11/12  by Michael Schaefers 



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.math_real.all;                            -- f�r random / uniform()
    
    
    
-- sg ::= Stimuli Generator    
entity sg is                                           -- Stimuli Generator
    port (
        txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
        clk    : out std_logic;                        -- CLocK
        nres   : out std_logic                         -- Not RESet ; low active reset
    );--]port
end entity sg;



architecture beh of sg is
    
    constant quarterClockCycle   : time := 25 ns;
    constant halfClockCycle      : time := 2*quarterClockCycle;
    constant fullClockCycle      : time := 2*halfClockCycle;
    
    signal   simulationRunning_s : boolean := true;
    
begin
    
	--txData <= "00";

    sg:                                                -- Stimuli Generator
    process is
        variable seed1_v : positive := 1;              -- 1 <= seed1 <= 2147483562  -> see IEEE.MATH_DEAL docu
        variable seed2_v : positive := 1;              -- 1 <= seed2 <= 2147483398  -> see IEEE.MATH_DEAL docu
        variable rRand_v : real;                       -- 
        variable iRand_v : integer;                    -- 
    begin
        simulationRunning_s <= true;
        
        wait for halfClockCycle;                       -- setup time
        txData <= (others=>'0');
        wait for halfClockCycle;                       -- synchronous reset gone (see "resGen")
        wait for fullClockCycle;
        wait for quarterClockCycle;                    -- "align" after rising clock edge
        
        txData <= "00";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        txData <= "11";  wait for fullClockCycle;      -- <<== MaRK
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        txData <= "11";  wait for fullClockCycle;      -- <<== MaRK
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        txData <= "11";  wait for fullClockCycle;      -- <<== MaRK
        txData <= "11";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "01";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "00";  wait for fullClockCycle;
        txData <= "10";  wait for fullClockCycle;
        txData <= "11";  wait for fullClockCycle;
        txData <= "01";  wait for fullClockCycle;
        txData <= "10";  wait for fullClockCycle;
        
        for i in 0 to 2000 loop
            uniform( seed1_v, seed2_v, rRand_v );      -- 0 < rRand < 1
            if    rRand_v < 0.25 then txData <= "00";
            elsif rRand_v < 0.5  then txData <= "01";
            elsif rRand_v < 0.75 then txData <= "10";
            else                      txdata <= "11";
            end if;
            wait for fullClockCycle;
        end loop;
        
        -- sg soll stoppen, sobald alle Testmuster abgearbeitet
        wait for 3*fullClockCycle;                     -- noch 10 Takte warten
        simulationRunning_s <= false;                  -- Stoppen
        wait;
    end process sg;
    
    
    
    clkGen:                                            -- CLocK GENerator
    process is
    begin
        clk <= '0';
        wait for fullClockCycle;
        while simulationRunning_s loop
            clk <= '1';
            wait for halfClockCycle;
            clk <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clkGen;
    
    
    
    resGen:                                            -- RESet GENerator
    process is
    begin
        nres <= '0';
        wait for 5*quarterClockCycle;
        nres <= '1';
        wait;
    end process resGen;
    
end architecture beh;
