library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       
    use ieee.std_logic_unsigned.all;

entity Testbench4Pattern is
end entity Testbench4Pattern;



architecture behaviour of Testbench4Pattern is

-- interne Signale der Testbench (Outputs des Stimuli-Generators und DUT-Outputs):

	signal clk_s	: std_logic;
	signal nres_s	: std_logic;
	signal txData_s : std_logic_vector(1 downto 0);

    --Mealy
	signal do_me  : std_logic_vector(1 downto 0);
	signal mrk_me : std_logic;
    --
	signal dip1_me	: std_logic_vector(1 downto 0); --Debug Shift Register
	signal dip2_me	: std_logic_vector(1 downto 0);
	signal dip3_me	: std_logic_vector(1 downto 0);
	signal dip4_me	: std_logic_vector(1 downto 0);
	signal dip5_me	: std_logic_vector(1 downto 0);
	signal dip6_me	: std_logic_vector(1 downto 0);

    --Moore
	signal do_mo  : std_logic_vector(1 downto 0);
	signal mrk_mo : std_logic;
    --
	signal dip1_mo	: std_logic_vector(1 downto 0); --Debug Shift Register
	signal dip2_mo	: std_logic_vector(1 downto 0);
	signal dip3_mo	: std_logic_vector(1 downto 0);
	signal dip4_mo	: std_logic_vector(1 downto 0);
	signal dip5_mo	: std_logic_vector(1 downto 0);
	signal dip6_mo	: std_logic_vector(1 downto 0);

	
	
--Komponenten:	

component sg                                         
    port (
        txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT (di)
        clk    : out std_logic;                        -- CLocK
        nres   : out std_logic                         -- Not RESet ; low active reset
    );--]port
end component sg;
for all: sg use entity work.sg(beh);


component Pattern_Recognition is
	port(
	    dip1 : out std_logic_vector(1 downto 0);
	    dip2 : out std_logic_vector(1 downto 0);
	    dip3 : out std_logic_vector(1 downto 0);
	    dip4 : out std_logic_vector(1 downto 0);
	    dip5 : out std_logic_vector(1 downto 0);
	    dip6 : out std_logic_vector(1 downto 0);
	    --
	    do	: out std_logic_vector(1 downto 0);
	    mrk	: out std_logic;
	    --
	    di : in std_logic_vector(1 downto 0);
	    --
	    clk	 : in std_logic;
	    nres : in std_logic
	);
end component Pattern_Recognition;
for Pattern_Recognition_Mealy_i: Pattern_Recognition use entity work.Pattern_Recognition(Pattern_Recognition_Mealy);
for Pattern_Recognition_Moore_i: Pattern_Recognition use entity work.Pattern_Recognition(Pattern_Recognition_Moore);


--Instanzen:
begin

	Stimuli_Generator_i : sg
		port map(
			txData => txData_s,
        	clk    => clk_s,
        	nres   => nres_s
		);

		
	
	Pattern_Recognition_Mealy_i: Pattern_Recognition
		port map(
			dip1 => dip1_me,
			dip2 => dip2_me,
			dip3 => dip3_me,
			dip4 => dip4_me,
			dip5 => dip5_me,
			dip6 => dip6_me,
			--
			do	=> do_me,
			mrk	=> mrk_me,
			--
			di => txData_s,
			--
			clk	 => clk_s,
			nres => nres_s
		);	

		
		
    Pattern_Recognition_Moore_i: Pattern_Recognition
		port map(
			dip1 => dip1_mo,
			dip2 => dip2_mo,
			dip3 => dip3_mo,
			dip4 => dip4_mo,
			dip5 => dip5_mo,
			dip6 => dip6_mo,
			--
			do	=> do_mo,
			mrk	=> mrk_mo,
			--
			di	=> txData_s,
			--
			clk	 => clk_s,
			nres => nres_s
		);
		
end architecture behaviour;