onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk /Testbench4Pattern/clk_s
add wave -noupdate -label di /Testbench4Pattern/txData_s
add wave -noupdate -label nres /Testbench4Pattern/nres_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate -divider -height 25 Mealy
add wave -noupdate -label do /Testbench4Pattern/do_me
add wave -noupdate -label mrk /Testbench4Pattern/mrk_me
add wave -noupdate -label {dip1: debug} /Testbench4Pattern/dip1_me
add wave -noupdate -label {dip2: debug delay 1} /Testbench4Pattern/dip2_me
add wave -noupdate -label {dip3: debug delay 2} /Testbench4Pattern/dip3_me
add wave -noupdate -label {dip4: debug delay 3} /Testbench4Pattern/dip4_me
add wave -noupdate -label {dip5: debug delay 4} /Testbench4Pattern/dip5_me
add wave -noupdate -label {dip6: debug delay 5} /Testbench4Pattern/dip6_me
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate -divider -height 25 Moore
add wave -noupdate -label do /Testbench4Pattern/do_mo
add wave -noupdate -label mrk /Testbench4Pattern/mrk_mo
add wave -noupdate -label {dip1: debug} /Testbench4Pattern/dip1_mo
add wave -noupdate -label {dip2: debug delay 1} /Testbench4Pattern/dip2_mo
add wave -noupdate -label {dip3: debug delay 2} /Testbench4Pattern/dip3_mo
add wave -noupdate -label {dip4: debug delay 3} /Testbench4Pattern/dip4_mo
add wave -noupdate -label {dip5: debug delay 4} /Testbench4Pattern/dip5_mo
add wave -noupdate -label {dip6: debug delay 5} /Testbench4Pattern/dip6_mo
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {134 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 184
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {3883 ns}
